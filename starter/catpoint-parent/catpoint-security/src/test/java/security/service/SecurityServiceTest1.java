package security.service;

import image.service.FakeImageService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.mockito.Mockito.*;

import org.mockito.Mock;
import security.application.StatusListener;
import security.data.*;

import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Set;

class SecurityServiceTest1 {
    private SecurityService securityService;
    private Sensor sensor = new Sensor("sensor1", SensorType.DOOR);
    private FakeImageService fakeImageService;
    private SecurityRepository securityRepository;

    @BeforeEach
    void setUp() {
        this.securityRepository = mock(PretendDatabaseSecurityRepositoryImpl.class);
        this.fakeImageService = mock(FakeImageService.class);
        this.securityService = new SecurityService(securityRepository, fakeImageService);
        securityService.addSensor(sensor);
    }

    //    1. If alarm is armed and a sensor becomes activated, put the system into pending alarm status.
    @Test
    public void whenAlarmIsArmedAwayAnd_sensorBecomesActivated() {
        when(securityRepository.getArmingStatus()).thenReturn(ArmingStatus.ARMED_HOME);
        when(securityRepository.getAlarmStatus()).thenReturn(AlarmStatus.NO_ALARM);
        securityService.changeSensorActivationStatus(sensor, true);
        verify(securityRepository).setAlarmStatus(AlarmStatus.PENDING_ALARM);
    }

    //    2. If alarm is armed and a sensor becomes activated and the system is already pending alarm, set the alarm status to alarm.
    @Test
    public void whenAlarmIsArmedAnd_sensorBecomesActivatedAnd_systemInPendingAlarm() {
        when(securityRepository.getArmingStatus()).thenReturn(ArmingStatus.ARMED_HOME);
        when(securityRepository.getAlarmStatus()).thenReturn(AlarmStatus.PENDING_ALARM);
        securityService.changeSensorActivationStatus(sensor, true);
        verify(securityRepository).setAlarmStatus(AlarmStatus.ALARM);
    }

    //   3. If pending alarm and all sensors are inactive, return to no alarm state.
    @Test
    public void whenPendingAlarmIsInactive_sensorsAreInactive_ReturnNoAlarm() {
        when(securityRepository.getAlarmStatus()).thenReturn(AlarmStatus.PENDING_ALARM);
        securityService.changeSensorActivationStatus(sensor, false);
        verify(securityRepository).setAlarmStatus(AlarmStatus.NO_ALARM);
    }

    //   4. If alarm is active, change in sensor state should not affect the alarm state.
    @ParameterizedTest
    @ValueSource(booleans = {true,false})
    public void alarmIsActive_sensorStateChange_NotEffectTheAlarmState(boolean state) {
        when(securityRepository.getAlarmStatus()).thenReturn(AlarmStatus.ALARM);
        securityService.changeSensorActivationStatus(sensor, state);
        verify(securityRepository, never()).setAlarmStatus(any(AlarmStatus.class));
    }

    //   5. If a sensor is activated while already active and the system is in pending state, change it to alarm state.
    @Test
    public void sensorIsActivatedWhileAlreadyActive_SystemInPendingState_ChangedToAlarmSate() {
        when(securityRepository.getAlarmStatus()).thenReturn(AlarmStatus.PENDING_ALARM);
        securityService.changeSensorActivationStatus(sensor, true);
        verify(securityRepository).setAlarmStatus(AlarmStatus.ALARM);
    }

    //   6. If a sensor is deactivated while already inactive, make no changes to the alarm state.
    @Test
    public void sensorIsDeactivatedWhileAlreadyInactive_NoChangeToAlarmState() {
        sensor.setActive(false);
        securityService.changeSensorActivationStatus(sensor, false);
        verify(securityRepository, never()).setAlarmStatus(any(AlarmStatus.class));
    }

    //   7. If the image service identifies an image containing a cat while the system is armed-home,
    //   put the system into alarm status.
    @Test
    public void imageContainACat_WhileSystemIsArmedHome_PutSystemInAlarmStatus() {
        BufferedImage bufferedImage = mock(BufferedImage.class);
        when(securityRepository.getArmingStatus()).thenReturn(ArmingStatus.ARMED_HOME);
        when(fakeImageService.imageContainsCat(bufferedImage, 50.0f)).thenReturn(true);
        securityService.processImage(bufferedImage);
        verify(securityRepository).setAlarmStatus(AlarmStatus.ALARM);
    }

    //   8. If the image service identifies an image that does not contain a cat, change the status to no alarm
    //   as long as the sensors are not active.
    @Test
    public void imageNotContainCat_ChangeStatusToNoAlarm_WhileSensorNotActive() {
        BufferedImage bufferedImage = mock(BufferedImage.class);
        when(fakeImageService.imageContainsCat(bufferedImage, 50.0f)).thenReturn(false);
        securityService.processImage(bufferedImage);
        sensor.setActive(false);
        verify(securityRepository).setAlarmStatus(AlarmStatus.NO_ALARM);
    }

    //    9. If the system is disarmed, set the status to no alarm.
    @Test
    public void setStatusToNoAlarm_WhenSystemIsDisarmed() {
        securityService.setArmingStatus(ArmingStatus.DISARMED);
        verify(securityRepository).setAlarmStatus(AlarmStatus.NO_ALARM);
    }

    //   10. If the system is armed, reset all sensors to inactive.
    @Test
    public void resetAllSensorToInactive_WhenAlarmStateIsPendingAnd_WhenSystemIsArmed() {
        Set<Sensor> sensorSet = new HashSet<>();
        sensorSet.add(sensor);
        when(securityRepository.getSensors()).thenReturn(sensorSet);
        when(securityRepository.getAlarmStatus()).thenReturn(AlarmStatus.PENDING_ALARM);
        securityService.setArmingStatus(ArmingStatus.ARMED_HOME);
        Set<Sensor> allSensors = securityService.getSensors();
        allSensors.forEach(a -> Assertions.assertFalse(a.getActive()));
    }

    //   11. If the system is armed-home while the camera shows a cat, set the alarm status to alarm.
    @Test
    public void systemIsArmedHome_WhileCameraShowACat_SetAlarmStatusToAlarm() {
        BufferedImage bufferedImage = mock(BufferedImage.class);
        when(fakeImageService.imageContainsCat(bufferedImage, 50.0f)).thenReturn(true);
        when(securityService.getArmingStatus()).thenReturn(ArmingStatus.ARMED_HOME);
        securityService.processImage(bufferedImage);
        verify(securityRepository).setAlarmStatus(AlarmStatus.ALARM);
    }

    //   Checking remove sensor function
    @Test
    public void removeSensorItemInList_FromOneItem_ToZeroItem() {
        securityService.removeSensor(sensor);
        Assertions.assertEquals(0, securityService.getSensors().size());
    }

    //  Alarm state and system is disarmed change alarm status
    @Test
    public void alarmStateAndSystemIsDisarmed_ChangeAlarmStatus() {
        when(securityRepository.getArmingStatus()).thenReturn(ArmingStatus.DISARMED);
        when(securityRepository.getAlarmStatus()).thenReturn(AlarmStatus.ALARM);
        securityService.changeSensorActivationStatus(sensor, true);
        verify(securityRepository).setAlarmStatus(AlarmStatus.PENDING_ALARM);
    }

    @Nested
    class RegisterStatusListenerTest {
        @Mock
        private StatusListener statusListener;
        @Test
        public void checkingAddStatusListener() {
            securityService.addStatusListener(statusListener);
        }

        @Test
        public void checkingRemoveStatusListener() {
            securityService.removeStatusListener(statusListener);
        }
    }
}