module catpoint.security {
    requires java.desktop;
    requires catpoint.image;
    requires com.google.common;
    requires com.google.gson;
    requires java.prefs;
    requires miglayout.swing;
    opens security.data to com.google.gson;
}